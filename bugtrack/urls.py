"""bugtrack URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from bug_app import views
# from django.urls import include

urlpatterns = [
    path('', views.index, name='homepage'),
    path('login/', views.login_view, name='login'),
    path('accounts/login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('ticket/<int:ticket_id>/', views.ticket_detail_view, name='ticketdetail'),
    path('addticket/', views.add_ticket_view, name='addticket'),
    path('assign/<int:ticket_id>/', views.assign_view, name='assignticket'),
    path('user/<str:username>/', views.user_detail_view, name='userdetail'),
    path('done/<int:ticket_id>/', views.done_view, name='done'),
    path('invalid/<int:ticket_id>/', views.invalid_view, name='invalid'),
    path('edit/<int:ticket_id>/', views.edit_ticket_view, name='edit'),
    path('admin/', admin.site.urls),
]

# urlpatterns += [
#     path('accounts/', include('django.contrib.auth.urls')),
# ]
