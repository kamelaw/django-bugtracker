"""
class CustomUser(AbstractUser):
    display_name = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.username


class Ticket(models.Model):
    # CHOICES
    NEW = 'NEW'
    INPROGRESS = 'INPROGRESS'
    DONE = 'DONE'
    INVALID = 'INVALID'

    STATUS_CHOICES = [
        (NEW, 'New'),
        (INPROGRESS, 'In Progress'),
        (DONE, 'Done'),
        (INVALID, 'Invalid')
    ]

    title = models.CharField(max_length=200)
    time_date = models.DateTimeField(default=timezone.now)
    description = models.TextField()
    ticket_status = models.CharField(max_length=11, choices=STATUS_CHOICES, default=NEW)
    filed_by = models.ForeignKey(CustomUser, related_name='filed_by', on_delete=models.CASCADE)
    assigned_to = models.ForeignKey(CustomUser, related_name='assigned_to', on_delete=models.CASCADE, null=True)
    completed_by = models.ForeignKey(CustomUser, related_name='completed_by', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.title

"""

from django import forms
from bug_app.models import CustomUser, Ticket


class LoginForm(forms.Form):
    username = forms.CharField(max_length=100)
    password = forms.CharField(widget=forms.PasswordInput)


class AddTicketForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = [
            'title',
            'description'
        ]


class EditTicketForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = [
            'title',
            'description'
        ]
